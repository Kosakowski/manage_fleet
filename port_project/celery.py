import os
from celery import Celery


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'port_project.settings')
# app = Celery('celery_app', backend='redis://localhost:6379/0', broker='redis://localhost:6379/0')
app = Celery('celery_app', backend='django-db', broker='redis://localhost:6379/0')
app.autodiscover_tasks()
