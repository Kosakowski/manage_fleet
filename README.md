# Port.app

**Running a project**

To run the project you need an isolated development environment, I'm using virtualenv here.
Below are also other commands useful at work.

 - Create a virtual environment: `python3 -m venv venv`
 - Activating the environment: `source venv/bin/activate`
- Installing libraries: `pip install -r requirements.txt`
- Database creation: `python manage.py migrate`
- Creating a superuser: `python manage.py createsuperuser`
- Generating a test code coverage report: `pytest --cov`
- Generating a code coverage report with tests in html: `pytest --cov --cov-report=html:coverage`
- `yapf` - Code formatting library according to PEP8

# Models

- **Manager** - manager managing the fleet 
- **Employee** - an employee who is to use the fleet of vehicles
- **Vehicle** - the vehicle with the coordinates where it is currently located, as well as the date fields when it was created and updated
- **Allocation** - assigning a vehicle to an employee, only the manager can do it.

# ToDo

The project is in the development phase, at the moment - locally.
When exposing project to development and production environments, must distinguish between the settings and libraries to install.
Files to create:
- `requirements_staging.txt`
- `requirements_production.txt`
- `settings_staging.py`
- `settings_production.py`
