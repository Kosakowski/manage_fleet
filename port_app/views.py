import datetime

from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.views import APIView
from rest_framework import status

from port_app.models import Vehicle, Employee, Allocation
from port_app.serializers import VehicleSerializer, EmployeeSerializer, AllocationSerializer
from port_app.decorators import manager_access_only


class APIRoot(APIView):

    def get(self, request, format=None):
        return Response(
            {
                'vehicles':
                reverse('vehicle-list', request=request, format=format),
                'employees':
                reverse('employee-list', request=request, format=format),
                'allocations':
                reverse('allocation-list', request=request, format=format)
            }, )


class EmployeeListView(generics.ListCreateAPIView):
    queryset = Employee.objects.all()
    serializer_class = EmployeeSerializer


class VehicleView(generics.ListCreateAPIView):
    queryset = Vehicle.objects.all()
    serializer_class = VehicleSerializer


#@method_decorator(cache_page(60*2), name='get')
class AllocationListView(generics.ListCreateAPIView):
    queryset = Allocation.objects.select_related('vehicle', 'employee').all()
    serializer_class = AllocationSerializer


class AllocationDateView(APIView):
    permission_classes = (IsAuthenticated, )
    serializer_class = AllocationSerializer
    queryset = Allocation.objects.select_related('vehicle', 'employee').all()

    @method_decorator(cache_page(60*2))
    def get(self, request, *args, **kwargs):
        code_date = self.kwargs['codedate']
        start_week = datetime.datetime.strptime(code_date + '-1',
                                                '%Y-W%W-%w').date()
        end_week = start_week + datetime.timedelta(days=6)
        allocations = self.queryset.filter(
            allocation_date__range=[start_week, end_week])
        if not allocations:
            return Response({'response': 'The object does not exist.'},
                            status=status.HTTP_400_BAD_REQUEST)
        serializer = self.serializer_class(allocations, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class AllocationDateDetailView(APIView):
    serializer_class = AllocationSerializer
    queryset = Allocation.objects.all()

    @method_decorator(manager_access_only())
    def get(self, request, *args, **kwargs):
        codedate = self.kwargs['codedate']
        start_week = datetime.datetime.strptime(codedate + '-1',
                                                '%Y-W%W-%w').date()
        end_week = start_week + datetime.timedelta(days=6)
        allocations = self.queryset.filter(
            id=self.kwargs['uuid'],
            allocation_date__range=[start_week, end_week])
        if not allocations:
            return Response({'response': 'The object does not exist.'},
                            status=status.HTTP_400_BAD_REQUEST)
        serializer = self.serializer_class(allocations, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @method_decorator(manager_access_only())
    def put(self, request, uuid, *args, **kwargs):
        uuid = self.kwargs['uuid']
        allocation_obj = self.queryset.get(id=uuid)
        if not allocation_obj:
            return Response({'response': 'The object does not exist.'},
                            status=status.HTTP_400_BAD_REQUEST)
        data = {
            'vehicle': request.data.get('vehicle'),
            'employee': request.data.get('employee'),
            'allocation_date': request.data.get('allocation_date')
        }
        serializer = self.serializer_class(instance=allocation_obj, data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
