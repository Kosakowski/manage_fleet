import uuid

from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):
    is_manager = models.BooleanField(default=False)
    is_employee = models.BooleanField(default=False)


class Manager(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL,
                                primary_key=True,
                                on_delete=models.CASCADE)

    def __str__(self):
        return self.user.username


class Employee(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL,
                                primary_key=True,
                                on_delete=models.CASCADE)
    role = models.CharField(max_length=200)
    is_blocked = models.BooleanField(default=False)

    def __str__(self):
        return self.user.username


class Vehicle(models.Model):
    id = models.UUIDField(primary_key=True,
                          default=uuid.uuid4,
                          editable=False,
                          unique=True)
    latitude = models.FloatField()
    longitude = models.FloatField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ["id"]

    def __str__(self):
        return str(self.id)


class Allocation(models.Model):
    id = models.UUIDField(primary_key=True,
                          default=uuid.uuid4,
                          editable=False,
                          unique=True)
    vehicle = models.ForeignKey(Vehicle,
                                on_delete=models.CASCADE,
                                null=True,
                                blank=True)
    employee = models.ForeignKey(Employee,
                                 on_delete=models.CASCADE,
                                 null=True,
                                 blank=True)
    allocation_date = models.DateField()

    class Meta:
        ordering = ["allocation_date"]

    def __str__(self):
        return str(self.id)
