from rest_framework import serializers
from .models import Vehicle, Employee, Allocation


class EmployeeSerializer(serializers.ModelSerializer):
    user = serializers.StringRelatedField()

    class Meta:
        model = Employee
        fields = ['user', 'role', 'is_blocked']


class VehicleSerializer(serializers.ModelSerializer):

    class Meta:
        model = Vehicle
        fields = ['id', 'latitude', 'longitude', 'created_at', 'updated_at']


class AllocationSerializer(serializers.ModelSerializer):
    employee = EmployeeSerializer()

    class Meta:
        fields = ['id', 'vehicle', 'employee', 'allocation_date']
        model = Allocation
