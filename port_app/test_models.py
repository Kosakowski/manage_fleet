import factory
import pytest
from datetime import date
from .factories import ManagerFactory, EmployeeFactory, VehicleFactory, AllocationFactory


@pytest.fixture
def uuid_mock():
    return factory.Faker('uuid4')


@pytest.mark.django_db
def test_manager_type_user():
    manager = ManagerFactory()
    assert manager.user.is_manager == True


@pytest.mark.django_db
def test_employee_type_user():
    employee = EmployeeFactory()
    assert employee.user.is_employee == True
    assert employee.role == "Driver"
    assert employee.is_blocked == False


@pytest.mark.django_db
def test_vehicle(uuid_mock):
    vehicle_obj = VehicleFactory(id=uuid_mock)
    assert vehicle_obj.latitude == -23.011492
    assert vehicle_obj.longitude == -53.515003


@pytest.mark.django_db
def test_allocation(uuid_mock):
    allocation_obj = AllocationFactory(id=uuid_mock)
    assert allocation_obj.allocation_date == date(2023, 1, 1)
