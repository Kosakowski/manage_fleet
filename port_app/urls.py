from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from port_app import views

urlpatterns = [
    path('employees/', views.EmployeeListView.as_view(), name='employee-list'),
    path('vehicles/', views.VehicleView.as_view(), name='vehicle-list'),
    path('allocations/',
         views.AllocationListView.as_view(),
         name='allocation-list'),
    path('allocations/<str:codedate>/',
         views.AllocationDateView.as_view(),
         name='allocation-date'),
    path('allocations/<str:codedate>/<uuid:uuid>/',
         views.AllocationDateDetailView.as_view(),
         name='allocation-datedetail'),
    path('', views.APIRoot.as_view(), name='api-root'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
