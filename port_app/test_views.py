from datetime import date

import pytest
from rest_framework.reverse import reverse
from rest_framework.test import APIClient
from port_app.factories import EmployeeFactory, VehicleFactory, AllocationFactory, ManagerFactory


@pytest.fixture
def api_client():
    return APIClient()


@pytest.mark.django_db
def test_employee_list(api_client):
    EmployeeFactory.create_batch(4)
    url = reverse('employee-list')
    response = api_client.get(url)
    assert response.status_code == 200
    assert len(response.data) == 4


@pytest.mark.django_db
def test_vehicle_list(api_client):
    VehicleFactory.create_batch(4)
    url = reverse('vehicle-list')
    response = api_client.get(url)
    assert response.status_code == 200
    assert len(response.data) == 4


@pytest.mark.django_db
def test_allocation_list(api_client):
    AllocationFactory.create_batch(4)
    url = reverse('allocation-list')
    response = api_client.get(url)
    assert response.status_code == 200
    assert len(response.data) == 4


@pytest.mark.django_db
def test_allocation_date(api_client):
    employee = EmployeeFactory()
    api_client.force_authenticate(employee.user)
    AllocationFactory(allocation_date=date(2023, 5, 19))
    AllocationFactory(allocation_date=date(2023, 5, 22))
    url = reverse('allocation-date', kwargs={'codedate': '2023-W20'})
    response = api_client.get(url)
    assert response.status_code == 200
    assert len(response.data) == 1
    assert str(date(2023, 5, 19)) in response.data[0]['allocation_date']
    assert str(date(2023, 5, 22)) not in response.data[0]['allocation_date']


@pytest.mark.django_db
def test_allocation_date_doesnt_exist(api_client):
    employee = EmployeeFactory()
    api_client.force_authenticate(employee.user)
    AllocationFactory(allocation_date=date(2023, 1, 1))
    url = reverse('allocation-date', kwargs={'codedate': '2033-W20'})
    response = api_client.get(url)
    assert response.status_code == 400
    assert len(response.data) == 1
    assert 'The object does not exist.' in response.data['response']


@pytest.mark.django_db
def test_allocation_detail(api_client):
    manager = ManagerFactory()
    api_client.force_authenticate(manager)
    AllocationFactory(id='2cb38847-8559-47cf-b761-482a10649880',
                      allocation_date=date(2023, 5, 19))
    AllocationFactory(id='029d4b47-d049-4aa8-83d9-755e209048d9',
                      allocation_date=date(2023, 5, 22))
    url = reverse('allocation-datedetail',
                  kwargs={
                      'codedate': '2023-W20',
                      'uuid': '2cb38847-8559-47cf-b761-482a10649880'
                  })
    response = api_client.get(url)
    assert response.status_code == 200
    assert len(response.data) == 1
    assert '2cb38847-8559-47cf-b761-482a10649880' in response.data[0]['id']
    assert '029d4b47-d049-4aa8-83d9-755e209048d9' not in response.data[0]['id']


@pytest.mark.django_db
def test_allocation_detail_doesnt_exist(api_client):
    manager = ManagerFactory()
    api_client.force_authenticate(manager)
    AllocationFactory(id='2cb38847-8559-47cf-b761-482a10649880',
                      allocation_date=date(2023, 1, 1))
    url = reverse('allocation-datedetail',
                  kwargs={
                      'codedate': '2023-W20',
                      'uuid': '2cb38847-8559-47cf-b761-482a10649880'
                  })
    response = api_client.get(url)
    assert response.status_code == 400
    assert len(response.data) == 1
    assert 'The object does not exist.' in response.data['response']


@pytest.mark.django_db
def test_allocation_detail_update_object(api_client):
    manager = ManagerFactory()
    api_client.force_authenticate(manager)
    vehicle_1 = VehicleFactory(id='05b4cee7-00c7-4ac3-a255-c7461ef7a093',
                               latitude=48.201566,
                               longitude=11.558913)
    vehicle_2 = VehicleFactory(id='f57ceb8c-6ab9-4036-87f9-4ec9ba3df25b',
                               latitude=51.198924,
                               longitude=6.721701)
    allocation_obj = AllocationFactory(
        id='2cb38847-8559-47cf-b761-482a10649880',
        vehicle=vehicle_1,
        allocation_date=date(2023, 5, 19))
    url = reverse('allocation-datedetail',
                  kwargs={
                      'codedate': '2023-W20',
                      'uuid': '2cb38847-8559-47cf-b761-482a10649880'
                  })
    response = api_client.get(url)
    assert response.status_code == 200
    assert len(response.data) == 1
    assert '2cb38847-8559-47cf-b761-482a10649880' in response.data[0]['id']
    assert allocation_obj.vehicle.id == vehicle_1.id

    allocation_obj.vehicle = vehicle_2
    url = reverse('allocation-datedetail',
                  kwargs={
                      'codedate': '2023-W20',
                      'uuid': '2cb38847-8559-47cf-b761-482a10649880'
                  })
    response = api_client.get(url)
    assert response.status_code == 200
    assert '2cb38847-8559-47cf-b761-482a10649880' in response.data[0]['id']
    assert allocation_obj.vehicle.id == vehicle_2.id
