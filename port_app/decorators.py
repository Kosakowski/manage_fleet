from functools import wraps
from rest_framework import status
from rest_framework.response import Response


def is_manager(person):
    if person.user.is_manager:
        return True
    return False


def manager_access_only():

    def decorator(view):

        @wraps(view)
        def _wrapped_view(request, *args, **kwargs):
            if not is_manager(request.user):
                return Response(
                    'You are not manager and you are not allowed to access this page!',
                    status=status.HTTP_403_FORBIDDEN)
            return view(request, *args, **kwargs)

        return _wrapped_view

    return decorator
