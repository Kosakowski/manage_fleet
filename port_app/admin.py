from django.contrib import admin
from .models import Manager, Employee, Vehicle, User, Allocation

admin.site.register(User)
admin.site.register(Manager)
admin.site.register(Employee)
admin.site.register(Vehicle)
admin.site.register(Allocation)
