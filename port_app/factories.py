import factory
from datetime import date

from .models import User, Manager, Employee, Vehicle, Allocation


class UserFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = User

    username = factory.Faker('email')
    is_manager = False
    is_employee = False


class ManagerFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = Manager

    user = factory.SubFactory(UserFactory, is_manager=True)


class EmployeeFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = Employee

    user = factory.SubFactory(UserFactory, is_employee=True)
    role = "Driver"
    is_blocked = False


class VehicleFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = Vehicle

    id = factory.Faker("uuid4")
    latitude = -23.011492
    longitude = -53.515003


class AllocationFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = Allocation

    id = factory.Faker("uuid4")
    vehicle = factory.SubFactory(VehicleFactory)
    employee = factory.SubFactory(EmployeeFactory)
    allocation_date = date(2023, 1, 1)
